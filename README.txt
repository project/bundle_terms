What is this module all about?
--------------------------------------------------------------

This module enables administrators to group entity bundles via taxonomy terms. Let's look at an example.

You've created a bunch of node types:
* Fire Trucks
* Fire Stations
* Firemen
* Police Cars
* Police Stations
* Policemen

You'd like to have a view that show all node types with tag 'Police' and another
view showing all node types with tag 'Fire'. Using Bundle Terms, you can create
those views without having to modify them each time that a new node type is
created.

Yes, you could do all of this without Bundle Terms, but you might have an
esoteric use case (like me) that just makes more sense with this module.

I wrote this before writing the module, to convince myself that it was worth it.


Why would I use this module? Doesn't taxonomy already do this?
--------------------------------------------------------------
Yes, the core taxonomy module can replicate this behavior, but it would require
about three times more data being saved to your database. For large sites, that
just doesn't make sense.

The real difference is this: Bundle terms allows you to save a term once per entity bundle. The core taxonomy module requires you to save a term once per entity.

To carry on with our example, Bundle Terms would make three basic data entries in order to tag Node Types" Fire Trucks, Fire Stations, and Firemen. Using core taxonomy, we would need to tag every single node of type Fire Trucks, Fire Stations, and Firemen.

What kind of black magic, Drupal sacrilege is this?
--------------------------------------------------------------

Wait! It's actually pretty Drupally. Here's what's going on behind the scenes:

* Bundle Terms defines a new 'bundle_terms_bundle' field type and creates a new
  field of that type (field_bundle_terms_bundles).
* A new bundle_terms vocabulary is created, and an instance of
  field_bundle_terms_bundles is added to it.
* A new 'Bundle Terms' fieldset is added to the node_type_form form. This
  fieldgroup contains a checkbox for each bundle_terms taxonomy term.
* When you save node_type_form, it updates each checked taxonomy term, modifying
  the value of field_bundle_terms_bundles for that term.

So what you're really doing is SETTING A FIELD VALUE ON A TAXONOMY TERM. That's
it. Not so crazy, right?


Views Integration
--------------------------------------------------------------

This module would be fairly useless without views integration. I highly
recommend enabling the default view that is shipped with this module,
nodes_by_bundle_term. If you'd like to set up your own view that can be filtered
by Bundle Terms, follow these steps:

* Create a new view with base table Node
* Add a "Content: Type: relationship
* Add a "Node Type: Bundle terms bundle" relationship
* Add a "Bundle Term Bundles: Bundle terms" relationship
* Add a "Taxonomy Term: Term" filter

You can now filter nodes by bundle terms.


Disclaimers
--------------------------------------------------------------
This module currently works only with nodes. It can be fairly easily extended
to other entity types by extending views integration.
