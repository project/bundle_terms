<?php

/**
 * @file
 * Views implementation for Bundle Terms module.
 */

/**
 * Implements hook_views_data().
 */
function bundle_terms_views_data() {
  $data = array();

  // Have a group defined will go into this field by default.
  $data['node_type']['table']['group'] = t('Node Type');

  // Advertise this table as a possible base table.
  $data['node_type']['table']['base'] = array(
    'field' => 'type',
    'title' => t('Node Type'),
    'weight' => -10,
    'defaults' => array(
      'field' => 'name',
    ),
  );

  // Type.
  $data['node_type']['type'] = array(
    'title' => t('Machine Name'),
    'help' => t('The machine name of the node type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    // Allows node_type table to be joined to
    // field_data_field_bundle_terms_bundles.
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'field_data_field_bundle_terms_bundles',
      'base field' => 'field_bundle_terms_bundles_value',
      'label' => t('bundle terms bundle'),
      'title' => t('Bundle terms bundle'),
      'help' => t('The Bundle Term bundle associated with the node'),
    ),
  );

  // Type.
  $data['node_type']['name'] = array(
    'title' => t('Name'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}


/**
 * Implements hook_views_data_alter().
 */
function bundle_terms_views_data_alter(&$data) {
  // Type. Allows node table to be joined to node_type table.
  $data['node']['type']['relationship'] = array(
    'handler' => 'views_handler_relationship',
    'base' => 'node_type',
    'base field' => 'type',
    'label' => t('node type'),
  );

  // Joins field_data_field_bundle_terms_bundles to taxonomy_term_data.
  $data['field_data_field_bundle_terms_bundles']['table']['group'] = t('Bundle Term Bundles');
  $data['field_data_field_bundle_terms_bundles']['entity_id']['relationship'] = array(
    'handler' => 'views_handler_relationship_bundle_terms_data',
    'base' => 'taxonomy_term_data',
    'base field' => 'tid',
    'label' => t('bundle terms'),
    'title' => t('Bundle terms'),
    'help' => t('The Bundle Term(s) associated with the node'),
  );
}
