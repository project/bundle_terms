<?php

/**
 * @file
 * Contains Display Suite customizations for Bundle Terms module.
 */

/**
 * Provides DS field callback for node help markup.
 */
function bundle_terms_ds_field($field) {
  if ($field['entity_type']) {
    $node = $field['entity'];

    $vid = variable_get('bundle_terms_vid', 0);
    $terms = taxonomy_term_load_multiple(array(), array('vid' => $vid));

    $output = '';
    // Build list of bundle term options.
    foreach ($terms as $term) {

      // Build list of default values (already selected).
      if ($term->field_bundle_terms_bundles) {
        foreach ($term->field_bundle_terms_bundles[LANGUAGE_NONE] as $value) {
          if ($value['value'] == $node->type) {
              unset($term->field_bundle_terms_bundles);
              $renderable_term = taxonomy_term_view($term);
              $output .= theme('bundle_term', $renderable_term);
          }
        }
      }
    }
  }

  return $output;
}
