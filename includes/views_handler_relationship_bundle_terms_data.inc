<?php

/**
 * @file
 *
 */

/**
 * Joins field_data_field_bundle_terms_bundles to taxonomy_term_data.
 *
 * @ingroup views_relationship_handlers
 */
class views_handler_relationship_bundle_terms_data extends views_handler_relationship {

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = 'taxonomy_term_data';
    $def['field'] = 'tid';

    $def['left_table'] = $this->relationship;
    $def['left_field'] = 'entity_id';

    $def['type'] = empty($this->options['required']) ? 'LEFT' : 'INNER';

    $join = new views_join();

    $join->definition = $def;
    $join->construct();
    $join->adjusted = TRUE;

    // use a short alias for this:
    $alias = $def['table'] . '_' . $this->table;

    $this->alias = $this->query->add_relationship($alias, $join, 'taxonomy_term_data', $this->relationship);
  }
}
